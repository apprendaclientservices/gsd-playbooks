[CmdletBinding()]
Param(
[string]$SubnetAddress,
[string]$SubnetMask
)

Import-Module ADDSDeployment

Add-DnsServerPrimaryZone `
-DynamicUpdate NonsecureAndSecure `
-NetworkId "$SubnetAddress/$SubnetMask" `
-ReplicationScope Domain `

