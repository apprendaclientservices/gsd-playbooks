[CmdletBinding()]
Param(
[string]$HostName
)
Rename-Computer -NewName $HostName -Force -PassThru

Start-Process "cmd.exe" "/c C:\Windows\Setup\Scripts\SetupComplete.cmd"
