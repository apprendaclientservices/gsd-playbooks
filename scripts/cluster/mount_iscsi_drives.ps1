[CmdletBinding()]
Param(
    [string]$fsIP,
    [string]$fsHost
)

Set-Service msiscsi -StartupType "Automatic"
Start-Service msiscsi

# Connect to ISCSI
New-IscsiTargetPortal -TargetPortalAddress $fsIP

$fsIQN1="iqn.1991-05.com.microsoft:"+$fsHost+"-data-target"
$fsIQN2="iqn.1991-05.com.microsoft:"+$fsHost+"-log-target"
$fsIQN3="iqn.1991-05.com.microsoft:"+$fsHost+"-dtc-target"

$drive="D"
Connect-IscsiTarget -NodeAddress $fsIQN1 -AuthenticationType None
Start-Sleep -Seconds 5
# Format the drive and bring it online

$drive="E"
Connect-IscsiTarget -NodeAddress $fsIQN2 -AuthenticationType None
Start-Sleep -Seconds 5
# Format the drive and bring it online

$drive="F"
Connect-IscsiTarget -NodeAddress $fsIQN3 -AuthenticationType None
Start-Sleep -Seconds 5
# Format the drive and bring it online

foreach($session in Get-IscsiSession) {
    Register-IscsiSession -SessionIdentifier $session.SessionIdentifier
}
