[CmdletBinding()]
Param(
  [string]$adminUser,
  [string]$adminPass,
  [string]$domainName,
  [string]$instanceHostname
)

winrm set winrm/config/winrs '@{MaxMemoryPerShellMB="3072"}'

Enable-WSManCredSSP -Force -Role client -DelegateComputer *
Enable-WSManCredSSP -Force -Role server

$securePassword = "$adminPass" | ConvertTo-SecureString -AsPlainText -force
$credential = New-Object System.Management.Automation.PsCredential("$domainName\$adminUser", $securePassword)

$session = New-PSSession -Authentication CredSSP -computername $instanceHostname -credential $credential
invoke-command -session $session -scriptblock {param($instanceHostname) start-job -name Installer -scriptblock { param($instanceHostname) Get-Cluster | Get-ClusterGroup "Available Storage" | Move-ClusterGroup -Node $instanceHostname }  -ArgumentList $instanceHostname} -ArgumentList $instanceHostname
invoke-command -session $session -command {$results = (wait-job -name Installer | receive-job); $results }

Remove-PSSession $session
Exit


