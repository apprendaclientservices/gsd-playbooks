#!/bin/sh

adduser ec2-user
usermod -G adm,wheel ec2-user
cp -r ~centos/.ssh/ ~ec2-user/
chown -R ec2-user ~ec2-user/
chgrp -R ec2-user ~ec2-user/
chown -R ec2-user ~ec2-user/.ssh/
chgrp -R ec2-user ~ec2-user/.ssh/

echo "ec2-user ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/100-cloud-init-users
