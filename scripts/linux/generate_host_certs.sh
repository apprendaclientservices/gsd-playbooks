#!/bin/sh


if [ $# -ne 2 ];
then
  echo "illegal number of parameters" && exit 1
fi

hostname=$1
password=$2

#Generate CSR for platform cert
openssl req -config /home/ec2-user/pcerts/openssl-$hostname.conf -new -newkey rsa:2048 -sha256 -keyout /home/ec2-user/pcerts/$hostname.key.pem -out /home/ec2-user/pcerts/$hostname.csr.pem -passin pass:$password -passout pass:$password -batch || exit 1

#Sign CSR with root ca to generate a signed SSL Platform Cert
openssl ca -batch -config /home/ec2-user/rootCertForPlatform/openssl-root.conf -extensions server_cert -days 375 -notext -md sha256 -in /home/ec2-user/pcerts/$hostname.csr.pem -out /home/ec2-user/pcerts/$hostname.cert.pem -passin pass:$password || exit 1

#Convert SSL Platform Cert+Key from PEM to PKCS12/PFX
openssl pkcs12 -export -out /home/ec2-user/pcerts/$hostname.pfx -CSP "Microsoft Enhanced Cryptographic Provider v1.0" -inkey /home/ec2-user/pcerts/$hostname.key.pem -in /home/ec2-user/pcerts/$hostname.cert.pem -passin pass:$password -passout pass:$password || exit 1
