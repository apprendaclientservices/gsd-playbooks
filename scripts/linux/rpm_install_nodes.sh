#!/bin/bash

if [ $# -ne 4 ];
then
  echo "illegal number of parameters" && exit 1
fi

wait_file() {
  local file="$1"; shift
  local wait_seconds="${1:-10}"; shift # 10 seconds as default timeout

  until test $((wait_seconds--)) -eq 0 -o -f "$file" ; do sleep 1; done

  ((++wait_seconds))
}

linuxVersion="rhel$1"
excludeVersion="rhel$2"
instance_hostname=$3
cloud_url=$4

cd /apprenda/repo/sys/*/System/Nodes/RPM || (echo "Failed to change to RPM path" && exit 2)
ls *.rpm | grep -v $excludeVersion | xargs sudo yum -y localinstall || (echo "Failed to install via rpm yum" && exit 3)

nohup /apprenda/apprenda-updater/bin/configure-node.sh -a /apprenda/repo/apps -s /apprenda/repo/sys -h $instance_hostname -o /tmp/output.log -c "http://$cloud_url" >/home/ec2-user/nohup.out 2>&1 &

wait_file /tmp/output.log 300
