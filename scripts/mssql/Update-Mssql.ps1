$spPath = "https://s3.amazonaws.com/gsd-automation-artifacts/published_artifacts/SQLServer2014SP2-KB3171021-x64-ENU.exe"
$outPath = "$PSScriptRoot\SQLSP2Update-x64.exe"

$client = New-Object System.Net.WebClient
$client.DownloadFile($spPath, $outPath)

& $outPath /q /IAcceptSQLServerLicenseTerms /Action=Patch /AllInstances
