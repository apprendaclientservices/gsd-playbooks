[CmdletBinding()]
Param(
    [string]$adminUser,
    [string]$adminPass,
    [string]$domainName,
    [string]$instanceHostname
)


$InstallSqlPs={
    $ErrorActionPreference = "Stop"
    $sqlServerIsoHttp = "https://s3.amazonaws.com/gsd-automation-artifacts/en_sql_server_2012_enterprise_edition_x86_x64_dvd_813294.iso"
    $sqlServerIso = "$PSScriptRoot\sql_server_2012_enterprise.iso"
    $client = New-Object System.Net.WebClient
    $client.DownloadFile($sqlServerIsoHttp, $sqlServerIso)
    $mountResult = Mount-DiskImage -ImagePath $sqlServerIso -PassThru

    $driveLetter = Get-Volume | Where-Object {$_.DriveType -eq 'CD-ROM'} | Select-Object -ExpandProperty DriveLetter
    if ($driveLetter.Count -lt 1) {
        Mount-DiskImage -ImagePath $fname
        $driveLetter = Get-Volume | Where-Object{$_.DriveType -eq 'CD-ROM'} | Select-Object -ExpandProperty DriveLetter
    }

    $installer = "$($driveLetter):\SETUP.EXE"
    $arguments =  '/ConfigurationFile=C:\Configuration_2.ini'
    $installResult = Start-Process $installer $arguments -Wait -ErrorAction Stop -PassThru
    $exitcode=$installResult.ExitCode
    if ($exitcode -ne 0 -and $exitcode -ne 3010) {
        Throw "SQL Server install failed with exit code $exitcode, check the installer logs for more details."
    }
}

Enable-WSManCredSSP -Force -Role client -DelegateComputer *
Enable-WSManCredSSP -Force -Role server

Remove-Item -Path "HKLM:\Software\Policies\Microsoft\Windows\CredentialsDelegation" -Recurse
new-Item -Name "CredentialsDelegation"  -Path "HKLM:\Software\Policies\Microsoft\Windows\" -type Directory
New-ItemProperty -Path "HKLM:\Software\Policies\Microsoft\Windows\CredentialsDelegation\" -Name "AllowFreshCredentials" -Value "1" -Type DWord
New-ItemProperty -Path "HKLM:\Software\Policies\Microsoft\Windows\CredentialsDelegation\" -Name "AllowFreshCredentialsWhenNTLMOnly" -Value "1" -Type DWord
New-ItemProperty -Path "HKLM:\Software\Policies\Microsoft\Windows\CredentialsDelegation\" -Name "ConcatenateDefaults_AllowFresh" -Value "1" -Type DWord
New-ItemProperty -Path "HKLM:\Software\Policies\Microsoft\Windows\CredentialsDelegation\" -Name "ConcatenateDefaults_AllowFreshNTLMOnly" -Value "1" -Type DWord
new-Item -Name "AllowFreshCredentials"  -Path "HKLM:\Software\Policies\Microsoft\Windows\CredentialsDelegation\" -type Directory
New-ItemProperty -Path "HKLM:\Software\Policies\Microsoft\Windows\CredentialsDelegation\AllowFreshCredentials\" -Name "1" -Value "wsman/*"
new-Item -Name "AllowFreshCredentialsWhenNTLMOnly"  -Path "HKLM:\Software\Policies\Microsoft\Windows\CredentialsDelegation\" -type Directory
New-ItemProperty -Path "HKLM:\Software\Policies\Microsoft\Windows\CredentialsDelegation\AllowFreshCredentialsWhenNTLMOnly\" -Name "1" -Value "wsman/*"

$securePassword = "$adminPass" | ConvertTo-SecureString -AsPlainText -force
$credential = New-Object System.Management.Automation.PsCredential("$domainName\$adminUser", $securePassword)

$Retries = 0
$Installed = $false
while (($Retries -lt 8) -and (!$Installed)) {
    try {
        Invoke-Command -Authentication Credssp -Scriptblock $InstallSqlPs -ComputerName $instanceHostname -Credential $credential
        $Installed = $true
        Dismount-DiskImage -ImagePath $sqlServerIso -PassThru
        New-Item c:\cluster_complete.txt -type file
    }
    catch {
        $Exception = $_
        $Retries++
        if ($Retries -lt 8) {
            Start-Sleep (([math]::pow($Retries, 2)) * 60)
        }
    }
}

