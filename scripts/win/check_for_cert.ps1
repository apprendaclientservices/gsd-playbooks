[CmdletBinding()]
Param(
  [string]$commonname
)

Get-ChildItem -Path Cert:\LocalMachine\My -DnsName "*$commonname*"
return (Get-ChildItem -Path Cert:\LocalMachine\My -DnsName "*$commonname*").Count -gt 0

