[CmdletBinding()]
Param(
  [string]$adminUser,
  [string]$domainName,
  [string]$adminPass,
  [string]$instanceHostname,
  [string]$IdentityHost,
  [string]$certFile,
  [string]$certPassword,
  [string]$coreDB
)

Install-WindowsFeature Windows-Internal-Database

Enable-WSManCredSSP -Force -Role client -DelegateComputer *
Enable-WSManCredSSP -Force -Role server

$secPass = "$adminPass" | ConvertTo-SecureString -AsPlainText -force
$creds = New-Object System.Management.Automation.PsCredential("$domainName\$adminUser", $secPass)
$session = New-PSSession -Authentication CredSSP -computername $instanceHostname -credential $creds

invoke-command -session $session -scriptblock {param($IdentityHost, $certFile, $certPassword, $creds, $coreDB) start-job -name Installer -scriptblock { param($IdentityHost, $certFile, $certPassword, $creds, $coreDB) $cert = New-Object System.Security.Cryptography.X509Certificates.X509Certificate2; $cert.Import($certFile,$certPassword,[System.Security.Cryptography.X509Certificates.X509KeyStorageFlags]"DefaultKeySet"); $CertificateThumbprint = $cert.thumbprint; Add-AdfsFarmNode -CertificateThumbprint $CertificateThumbprint -ServiceAccountCredential $creds -SQLConnectionString "Data Source=$coreDB;Integrated Security=True" }  -ArgumentList $IdentityHost, $certFile, $certPassword, $creds, $coreDB} -ArgumentList $IdentityHost, $certFile, $certPassword, $creds, $coreDB

invoke-command -session $session -command { $results = (Wait-Job -name Installer | Receive-Job); $results}
Remove-PSSession $session

