[CmdletBinding()]
Param(
    [string]$adminUser,
    [string]$adminPass,
    [string]$domainName,
    [string]$instanceHostname,
    [string]$certSubject,
    [string]$userAccount
)

Enable-WSManCredSSP -Force -Role client -DelegateComputer *
Enable-WSManCredSSP -Force -Role server

$securePassword = "$adminPass" | ConvertTo-SecureString -AsPlainText -force
$credential = New-Object System.Management.Automation.PsCredential("$domainName\$adminUser", $securePassword)

$session = New-PSSession -Authentication CredSSP -computername $instanceHostname -credential $credential
invoke-command -session $session -scriptblock {param($certSubject, $userAccount) start-job -name CertKey -scriptblock { param($certSubject, $userAccount) & "C:/CertificatePrivateKey.exe" $certSubject $userAccount 2>&1 | Out-String }  -ArgumentList $certSubject, $userAccount} -ArgumentList $certSubject, $userAccount
invoke-command -session $session -command {$results = (wait-job -name CertKey | receive-job); $results }

Remove-PSSession $session
Exit
