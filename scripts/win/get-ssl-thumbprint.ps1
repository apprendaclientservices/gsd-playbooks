[CmdletBinding()]
Param(
  [string]$certFile,
  [string]$certPassword
)

$cert = New-Object System.Security.Cryptography.X509Certificates.X509Certificate2
$cert.Import($certFile,$certPassword,[System.Security.Cryptography.X509Certificates.X509KeyStorageFlags]"DefaultKeySet")
$cert.thumbprint
