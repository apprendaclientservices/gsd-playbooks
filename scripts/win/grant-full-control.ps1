param
(
	[String[]] $paths,
	[String[]] $users
)

foreach ($path in $paths) 
{
	$acl = Get-Acl $path
	foreach ($user in $users)
	{
		Write-Host "Granting access to $path for $user"
		$inheritance = [System.Security.AccessControl.InheritanceFlags]::ContainerInherit -bor [System.Security.AccessControl.InheritanceFlags]::ObjectInherit
		$propagation = [System.Security.AccessControl.PropagationFlags]::InheritOnly
		$accessRule = New-Object system.security.accesscontrol.filesystemaccessrule($user,"FullControl", $inheritance, $propagation,"Allow")
		$acl.AddAccessRule($accessRule)
	}
	Set-Acl $path $acl
}