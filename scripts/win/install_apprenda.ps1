[CmdletBinding()]
Param(
    [Parameter(Mandatory=$true)]
    [string]
    $adminUser,
    
    [Parameter(Mandatory=$true)]
    [string]
    $adminPass,

    [Parameter(Mandatory=$true)]
    [string]
    $domainName,

    [Parameter(Mandatory=$true)]
    [string]
    $instanceHostname,

    [Parameter(Mandatory=$true)]
    [string]
    $environmentName
)

winrm set winrm/config/winrs '@{MaxMemoryPerShellMB="3072"}'
Enable-WSManCredSSP -Force -Role client -DelegateComputer *
Enable-WSManCredSSP -Force -Role server

$InstallerScriptBlock = {param($environmentName)
    $installer = "C:/apprendafrom_unpacked/Installer/Apprenda.Wizard.exe"
    $arguments = -Join('Install -inputFile c:/', $environmentName, '-input.xml -autoRetry -autoRepair')
    $installResult = Start-Process $installer $arguments -Wait -ErrorAction Stop -PassThru
    $exitcode=$installResult.ExitCode
    if ($exitcode -ne 0) {
        Throw "ACP install failed with exit code $exitcode, check the installer logs for more details."
    }
}

$securePassword = "$adminPass" | ConvertTo-SecureString -AsPlainText -force
$credential = New-Object System.Management.Automation.PsCredential("$domainName\$adminUser", $securePassword)

$Retries = 0
$Installed = $false
while (($Retries -lt 3) -and (!$Installed)) {
    try {
        Invoke-Command -Authentication Credssp -ComputerName $instanceHostname -Credential $credential -Scriptblock $InstallerScriptBlock -ArgumentList $environmentName
        $Installed = $true
    }
    catch {
        $Exception = $_
        $Retries++
        if ($Retries -lt 3) {
            Start-Sleep (([math]::pow($Retries, 2)) * 30)
        }
    }
}

if (!$Installed) {
        throw $Exception
}

Exit

